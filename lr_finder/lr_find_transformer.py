#!/usr/local/bin/python

import os
import sys

project_path = os.path.sep.join(os.path.abspath(__file__).split(os.path.sep)[:-2])
if project_path not in sys.path:
    sys.path.append(project_path)

import math
import tensorflow as tf
import keras
import argparse
import numpy as np
from keras.callbacks import ReduceLROnPlateau
from DNN_Learning_Rate.lr_callbacks import LR_Finder
from models.transformer import TransformerModel
from commonly_used_code import helper_fn
from run_script.args_parser import transformer_add_arguments
from example.data_reader import DataSet

#import keras.backend.tensorflow_backend as KTF
#KTF.set_session(tf.Session(config=tf.ConfigProto(device_count={'cpu':0})))
os.environ["CUDA_VISIBLE_DEVICES"] = "2"

class Transformer:
    def __init__(self, args):
        # real Transformer model architecture
        self.transformer_model = TransformerModel(args=args,
                                                  transformer_dropout=0.05,
                                                  embedding_dropout=0.05,
                                                  use_same_embedding=False,
                                                  )
        self.args = args
        exp_name = 'transformer_lr_finder'
        exp_name = args.data_set + '_' + exp_name + '.lr'

        self.lr_finder_dir = './result_lr_finder' 
        helper_fn.makedirs(self.lr_finder_dir)
        self.lr_finder_path = os.path.join(self.lr_finder_dir, exp_name)

    def train(self, step):
        ds = DataSet(self.args)
        print('*' * 100)
        print('train sample number: ', ds.train_sample_num)
        print('valid sample number: ', ds.valid_sample_num)
        print('test sample number: ', ds.test_sample_num)
        print('current iteration turn: %s/%s' % (step + 1, self.args.test_times))
        print('*' * 100)

        train_generator = ds.data_generator('train', 'transformer',
                            max_src_len=self.args.src_seq_length, 
                            max_tar_len=self.args.tar_seq_length, 
                            )

        valid_generator = ds.data_generator('valid', 'transformer',
                            max_src_len=self.args.src_seq_length, 
                            max_tar_len=self.args.tar_seq_length, 
                            )

        def compile_new_model():
            _model = self.transformer_model.get_model(ds.pad_id)
            _model.compile(
                           optimizer=keras.optimizers.Adam(),
                           loss = keras.losses.sparse_categorical_crossentropy,
                          )
            return _model

        print('Compile new model...')
        model = compile_new_model()

        step_num = ds.train_sample_num // self.args.batch_size
        verbose = 1
        base_lr = 1e-9
        max_lr =20
        multiplier = math.pow(max_lr / base_lr, (1.0 / (step_num - 1)))
        lr_finder = LR_Finder(base_lr=base_lr, lr_multiplier=multiplier, verbose=verbose)

        lrate = ReduceLROnPlateau(
                                  monitor='val_loss', 
                                  factor=0.5, 
                                  patience=self.args.lr_decay_patience, 
                                  verbose=verbose, 
                                  mode='auto', 
                                  min_delta=0.0001, 
                                  cooldown=0, 
                                  min_lr=self.args.lr_min
                                  )
        callback_list = [lr_finder]
    
        hist = model.fit_generator(
                        generator=train_generator, 
                        steps_per_epoch=(ds.train_sample_num//self.args.batch_size),
                        epochs=1,
                        callbacks=callback_list, 
                       )

        save_file = self.lr_finder_path + str(step)
        lr_finder.save_info(save_file)

    def get_final_lr_value(self):
        print('Generating final lr score...')
        files = []
        for i in range(args.test_times):
            save_file = self.lr_finder_path + str(i)
            files.append(save_file)

        avg_lr_index = 0
        avg_lr = 0
        avg_loss = 0
        for file_name in files:
            print('Reading %s...' % file_name)
            with open(file_name) as f:
                for index, line in enumerate(f):
                    elems = line.strip().split('\t')
                    if index == 7:
                        best_lr_index = int(elems[0])
                        best_lr = float(elems[1])
                        min_loss = float(elems[2])
                        avg_lr_index += best_lr_index
                        avg_lr += best_lr
                        avg_loss += min_loss
                        break
        count = len(files)
        avg_lr_index /= (count * 1.0)
        avg_lr /= (count * 1.0)
        avg_loss /= (count * 1.0)
        seq = []

        final_lr_index = 'average lr index: ' + str(avg_lr_index)
        final_lr = 'averate lr score: ' + str(avg_lr)
        final_loss = 'averate loss score: ' + str(avg_loss)

        print(final_lr_index) 
        print(final_lr)
        print(final_loss)

        seq.append(final_lr_index) 
        seq.append(final_lr)
        seq.append(final_loss)

        final_outobj = open(self.lr_finder_path, 'w')
        write = '\n'.join(seq)
        final_outobj.write(write)
        final_outobj.close()
        print('Done!')

if __name__ == '__main__':
    if len(sys.argv) != 2:
        raise ValueError('Please give a test_times, e.g. --test_times=10 ')
    parser = argparse.ArgumentParser()
    transformer_add_arguments(parser)
    parser.add_argument("--test_times", default=10, type=int, help="How many times should the LR_finder be ran.")
    args = parser.parse_args()

    # get the average lr score of the test_times
    for i in range(args.test_times):
        trans = Transformer(args)
        trans.train(i)
    trans.get_final_lr_value()




