# cikm-2019-repo

## TO DO:
1. Multi-task models. Re-implement this model with new framework.
2. The paper: Learning to Transform, Combine and Reason model. This is multi-hop Transformer model.
3. Our own Transformer-based model

## Models
1. **Memory Neural Network.**
Traditionally, the MemNN model predicts a series of words' probabilities and use them as the final outputs, which works well on bAbI data set. In our setting, we need to generate a sequence on the decoder side instead of just one word. So we don't use the vanilla MemNN output. We take the output of the MemNN as the encoder output and it will be input to another RNN decoder (GRU cell) to generate a sequence.

## folder: configuration
This is mainly for file path configuration. All data path can be configed in config.ini

## folder: commonly_used_code
Include file path config and commonly used code in helper_fn.py

## folder: pre_processing
This is mainly for data extraction. Currently, there are two data sets: Reddit and Wizard of Wikipedia. Both of them can be pre-precessed here.

## folder: seq2seq_model_context_facts
This is for previous models. 


## Reference and Acknowledgement:
1. Pointer_generator: Thank Abisee, we use his repo on the Github. Obeying the Apache License version 2.0, we just use this repo for researching. The original repo can be found here: [Pointer-Generator](https://github.com/abisee/pointer-generator)
2. Memory Neural Network: Thank these authors. We inspire from two repos, [memn2n tensorflow version](https://github.com/domluna/memn2n) and [memn2n keras version](https://github.com/IliaGavrilov/ChatBotEndToEndMemoryNeuralNet)
3. lr_finder: Thanks Drxan for the DNN_Learning_Rate repo. I use this to find the best initial Learning Rate. The original repo can be found here: [DNN_Learning_Rate](https://github.com/Drxan/DNN_Learning_Rate).
4. Transformer and Universal Transformer: Thanks kpot for his repo: keras-transformer. I changed his original code to fit with my experiments. He also implemented BERT on top of this repo. Even this repo didn't implement entire Transformer, it still easy to add Decoder part in this framework. The original repo can be found here: [keras-transformer](https://github.com/kpot/keras-transformer).


