import os, sys, time, math

project_path = os.path.sep.join(os.path.abspath(__file__).split(os.path.sep)[:-2])
if project_path not in sys.path:
    sys.path.append(project_path)

import tensorflow as tf
import keras
import argparse
import numpy as np
from keras.callbacks import ModelCheckpoint, EarlyStopping, LearningRateScheduler, ReduceLROnPlateau
from keras.utils import plot_model
from keras.models import load_model
from keras.utils import get_custom_objects
from models.rear_vspg_transformer import VSPGTransformerModel
from commonly_used_code import helper_fn, config
from run_script.args_parser import vspg_transformer_add_arguments
from vspgt_data_reader import DataSet

import keras.backend.tensorflow_backend as KTF
KTF.set_session(tf.Session(config=tf.ConfigProto(device_count={'cpu':0})))
#os.environ["CUDA_VISIBLE_DEVICES"] = "0"

class VSPGTransformer:
    def __init__(self, args):
        # real Transformer model architecture
        self.transformer_model = VSPGTransformerModel(args=args,
                                                  transformer_dropout=0.05,
                                                  embedding_dropout=0.05,
                                                  use_same_embedding=True,
                                                  )
        self.args = args
        exp_name = args.data_set + '_' + args.exp_name

        # create experiment dir
        self.exp_dir= os.path.join(args.checkpoints_dir, exp_name)
        helper_fn.makedirs(self.exp_dir)
        hist_name = exp_name + '.hist'
        model_name = exp_name + '_final_model.h5'
        #model_name = 'model-ep039-loss4.265-val_loss4.477.h5'

        self.history_path = os.path.join(self.exp_dir, hist_name)
        self.model_path = os.path.join(self.exp_dir, model_name)
        
        outputs_dir = args.outputs_dir
        helper_fn.makedirs(outputs_dir)
        self.src_out_name = exp_name + '.src'
        self.src_out_path = os.path.join(outputs_dir, self.src_out_name)
        self.pred_out_name = exp_name + '.pred'
        self.pred_out_path = os.path.join(outputs_dir, self.pred_out_name)
        self.tar_out_name = exp_name + '.tgt'
        self.tar_out_path = os.path.join(outputs_dir, self.tar_out_name)

    def train(self):
        ds = DataSet(self.args)
        print('*' * 100)
        print('train sample number: ', ds.train_sample_num)
        print('valid sample number: ', ds.valid_sample_num)
        print('test sample number: ', ds.test_sample_num)
        print('*' * 100)

        train_generator = ds.data_generator('train', 'vspg_transformer')

        valid_generator = ds.data_generator('valid', 'vspg_transformer')

        def compile_new_model():
            _model = self.transformer_model.get_model(ds.pad_id)
            _model.compile(
                           optimizer=keras.optimizers.Adam(lr=self.args.lr),
                           loss = keras.losses.sparse_categorical_crossentropy,
                           #loss = None,
                          )
            return _model

        if os.path.exists(self.model_path):
            #raise ValueError('Current model just saves weights. Please re-train the model.')
            print('Loading model from: %s' % self.model_path)
            custom_dict = get_custom_objects()
            model = load_model(self.model_path, custom_objects=custom_dict)
        else:
            print('Compile new model...')
            model = compile_new_model()

        model.summary()
        #plot_model(model, to_file='model_structure.png',show_shapes=True)

        verbose = 1
        earlystopper = EarlyStopping(monitor='val_loss', patience=self.args.early_stop_patience, verbose=verbose)
        ckpt_name = 'model-ep{epoch:03d}-loss{loss:.3f}-val_loss{val_loss:.3f}.h5'
        ckpt_path = os.path.join(self.exp_dir, ckpt_name)
        #checkpoint = ModelCheckpoint(ckpt_path, monitor='val_loss', verbose=verbose, save_weights_only=True, save_best_only=True, mode='min')
        checkpoint = ModelCheckpoint(ckpt_path, monitor='val_loss', verbose=verbose, save_best_only=True, mode='min')
        lrate = keras.callbacks.ReduceLROnPlateau(
                                     monitor='val_loss', 
                                     factor=0.5, 
                                     patience=self.args.lr_decay_patience, 
                                     verbose=verbose, 
                                     mode='auto', 
                                     min_delta=0.0001, 
                                     cooldown=0, 
                                     min_lr=self.args.lr_min,
                                     )

        callback_list = [earlystopper, checkpoint, lrate]
        #callback_list = [earlystopper, lrate]
        #callback_list = [checkpoint, lrate]
    
        hist = model.fit_generator(
                        generator=train_generator, 
                        steps_per_epoch=(ds.train_sample_num//self.args.batch_size),
                        epochs=self.args.epochs,
                        callbacks=callback_list, 
                        validation_data=valid_generator,
                        validation_steps=(ds.valid_sample_num//self.args.batch_size),
                       )
        with open(self.history_path,'w') as f:
            f.write(str(hist.history))

        model.save(self.model_path)
        #model.save_weights(self.model_path)
        #plot_model(model, to_file='model_structure.png',show_shapes=True) 

    def test(self):
        ds = DataSet(args)
        test_generator = ds.data_generator('test', 'vspg_transformer')

        def compile_new_model():
            _model = self.transformer_model.get_model(ds.pad_id)
            _model.compile(
                           optimizer=keras.optimizers.Adam(lr=self.args.lr),
                           loss = None,
                           #loss = keras.losses.sparse_categorical_crossentropy,
                          )
            return _model

        # load_model
        print('Loading model from: %s' % self.model_path)
        custom_dict = get_custom_objects()
        model = load_model(self.model_path, custom_objects=custom_dict)
        #model = compile_new_model()
        #model.load_weights(self.model_path)


        src_outobj = open(self.src_out_path, 'w')
        pred_outobj = open(self.pred_out_path, 'w')
        tar_outobj = open(self.tar_out_path, 'w')

        for batch_index, ([src_input, tar_input, facts_input, \
            src_input_exp, facts_input_exp], _, oov_dict) in enumerate(test_generator):
            if batch_index > (ds.test_sample_num // self.args.batch_size):
                # finish all of the prediction
                break
            print('Current batch: {}/{}. '.format(batch_index, ds.test_sample_num // self.args.batch_size))
            cur_batch_size = tar_input.shape[0]
            tar_length = tar_input.shape[1]

            results = np.zeros_like(tar_input)
            results[:, 0] = ds.start_id
            for i in range(1, tar_length):
                results[:, i] = ds.pad_id

            results_p = np.zeros_like(tar_input)
            results_p[:, 0] = ds.start_id
            for i in range(1, tar_length):
                results_p[:, i] = ds.pad_id

            for t in range(1, tar_length):
                preds = model.predict([src_input, np.asarray(results_p), facts_input, src_input_exp, facts_input_exp]) 
                pred_id = np.argmax(preds, axis=-1)
                cur_pred_id = []
                for _id in pred_id[:, t-1]:
                    token = ds.tar_id_tokens.get(int(_id), config.UNK_TOKEN)
                    if token == config.UNK_TOKEN:
                        cur_pred_id.append(ds.unk_id)
                    else:
                        cur_pred_id.append(_id)
                results_p[:, t] = np.asarray(cur_pred_id)
                results[:, t] = np.asarray(pred_id[:, t-1])

            oov_token2id = []
            for oov_d in oov_dict:
                t_dict = dict()
                for key, value in oov_d.items():
                    t_dict[value] = key
                oov_token2id.append(t_dict) 

            def output_results(tag, outputs, outobj):
                for out_index, result in enumerate(outputs):
                    seq = []
                    for _id in result:
                        _id = int(_id)
                        if _id == ds.end_id:
                            break
                        if _id != ds.pad_id and _id != ds.start_id:
                        #if _id != ds.pad_id:
                            token = ds.tar_id_tokens.get(_id, config.UNK_TOKEN)
                            #seq.append(ds.tar_id_tokens.get(_id, config.UNK_TOKEN))
                            if tag == 'result' and token == config.UNK_TOKEN:
                                token = oov_token2id[out_index].get(_id, config.UNK_TOKEN)
                            seq.append(token)
                    write_line = ' '.join(seq)
                    #if tag == 'result':
                    #    print(write_line)
                    write_line = write_line + '\n'
                    outobj.write(write_line)
    
            output_results('result', results, pred_outobj)
            output_results('src', src_input, src_outobj)
            output_results('tar', tar_input, tar_outobj)
    
        src_outobj.close()
        pred_outobj.close()
        tar_outobj.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    vspg_transformer_add_arguments(parser)
    args = parser.parse_args()
    print(args)

    trans = VSPGTransformer(args)
#    trans.train()
    trans.test()


