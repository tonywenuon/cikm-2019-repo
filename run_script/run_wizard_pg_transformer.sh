#!/bin/bash

cd ../example
python_exe='train_pg_transformer.py' 
#python $python_exe \
#       --data_set=wizard \
#       --exp_name='pg_transformer_fact1' \
#       --epochs=100 \
#       --batch_size=32 \
#       --src_seq_length=30 \
#       --tar_seq_length=30 \
#       --embedding_dim=100 \
#       --num_heads=4 \
#       --transformer_depth=1 \
#       --fact_number=1 \
#       --vocab_size=50000 \
#       --early_stop_patience=5 \
#       --lr=0.0037 \
#       --lr_min=0.00001 \
#       --lr_decay_patience=3 \
#       --checkpoints_dir='log' \
#       --outputs_dir='outputs' 

python $python_exe \
       --data_set=wizard \
       --exp_name='pg_transformer_fact2' \
       --epochs=100 \
       --batch_size=32 \
       --src_seq_length=30 \
       --tar_seq_length=30 \
       --embedding_dim=100 \
       --num_heads=4 \
       --transformer_depth=1 \
       --fact_number=2 \
       --vocab_size=50000 \
       --early_stop_patience=5 \
       --lr=0.00153 \
       --lr_min=0.00001 \
       --lr_decay_patience=3 \
       --checkpoints_dir='log' \
       --outputs_dir='outputs' 

python $python_exe \
       --data_set=wizard \
       --exp_name='pg_transformer_fact3' \
       --epochs=100 \
       --batch_size=32 \
       --src_seq_length=30 \
       --tar_seq_length=30 \
       --embedding_dim=100 \
       --num_heads=4 \
       --transformer_depth=1 \
       --fact_number=3 \
       --vocab_size=50000 \
       --early_stop_patience=5 \
       --lr=0.00153 \
       --lr_min=0.00001 \
       --lr_decay_patience=3 \
       --checkpoints_dir='log' \
       --outputs_dir='outputs' 

python $python_exe \
       --data_set=wizard \
       --exp_name='pg_transformer_fact4' \
       --epochs=100 \
       --batch_size=32 \
       --src_seq_length=30 \
       --tar_seq_length=30 \
       --embedding_dim=100 \
       --num_heads=4 \
       --transformer_depth=1 \
       --fact_number=4 \
       --vocab_size=50000 \
       --early_stop_patience=5 \
       --lr=0.00153 \
       --lr_min=0.00001 \
       --lr_decay_patience=3 \
       --checkpoints_dir='log' \
       --outputs_dir='outputs' 


