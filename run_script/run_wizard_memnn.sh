#!/bin/bash

cd ../example
python_exe='train_memnn.py' 
python $python_exe \
       --data_set=wizard \
       --exp_name='wizard_memnn' \
       --epochs=100 \
       --batch_size=10 \
       --hops=3 \
       --src_seq_length=30 \
       --tar_seq_length=30 \
       --fact_seq_length=30 \
       --embedding_dim=100 \
       --hidden_dim=100 \
       --vocab_size=50000 \
       --early_stop_patience=5 \
       --lr_min=0.005 \
       --lr_decay_patience=3 \
       --fact_number=10 \
       --checkpoints_dir='log' \
       --outputs_dir='outputs' 



