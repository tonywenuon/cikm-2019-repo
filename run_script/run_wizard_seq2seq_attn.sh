#!/bin/bash

cd ../example
python_exe='train_seq2seq_attn.py' 
python $python_exe \
       --data_set=wizard \
       --exp_name='wizard_seq2seq_attn_2' \
       --epochs=100 \
       --batch_size=50 \
       --src_seq_length=30 \
       --tar_seq_length=30 \
       --embedding_dim=200 \
       --hidden_dim=200 \
       --display_step=50 \
       --early_stop_patience=5 \
       --fact_number=10 \
       --conv_number=10 \
       --checkpoints_dir='log' \
       --tensorboard_dir='tensorboard' \
       --outputs_dir='outputs' \



