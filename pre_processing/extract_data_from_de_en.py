import os, sys, time, math

project_path = os.path.sep.join(os.path.abspath(__file__).split(os.path.sep)[:-2])
if project_path not in sys.path:
    sys.path.append(project_path)

import queue
import re
from commonly_used_code import helper_fn, config

class ExtractData:
    def __init__(self, 
                 train_de_data_path, 
                 train_en_data_path, 
                 valid_de_data_path, 
                 valid_en_data_path, 
                 test_de_data_path, 
                 test_en_data_path, 
                 train_qa_data_path,
                 valid_qa_data_path,
                 test_qa_data_path,
                 src_global_token_path=None,
                 tar_global_token_path=None 
                 ):

        self.QA_UNK_RATE = 0.1 # the upper bound of UNK in question and answer
        self.MAX_VOCAB_SIZE = 50000
        # input data
        self.train_de_data_path = train_de_data_path
        self.train_en_data_path = train_en_data_path
        self.valid_de_data_path = valid_de_data_path
        self.valid_en_data_path = valid_en_data_path
        self.test_de_data_path = test_de_data_path
        self.test_en_data_path = test_en_data_path

        # generate data
        self.src_global_token_path = src_global_token_path
        self.tar_global_token_path = tar_global_token_path
        self.train_qa_data_path = train_qa_data_path
        self.valid_qa_data_path = valid_qa_data_path
        self.test_qa_data_path = test_qa_data_path

    def __init_outobjs(self):
        self.dic_qa_objs = dict()
        self.dic_qa_objs['train'] = open(self.train_qa_data_path, 'w')
        self.dic_qa_objs['valid'] = open(self.valid_qa_data_path, 'w')
        self.dic_qa_objs['test'] = open(self.test_qa_data_path, 'w')

    def __close_outobjs(self):
        for key in self.dic_qa_objs.keys():
            self.dic_qa_objs[key].close()

    def __generate_global_token(self):
        print('generating global token...')
        src_obj = open(self.src_global_token_path, 'w')
        tar_obj = open(self.tar_global_token_path, 'w')
        dic_src_tokens = {}
        dic_src_tokens_tmp = {}
        dic_tar_tokens = {}
        dic_tar_tokens_tmp = {}
        index = 0
        for token in config.SPECIAL_TOKENS:
            dic_src_tokens[token] = index
            dic_tar_tokens[token] = index
            index += 1

        print(self.train_qa_data_path)
        with open(self.train_qa_data_path) as f:
            for line in f:
                elems = line.strip().split('\t')
                src = elems[1]
                tar = elems[2]

                tokens = src.strip().split(' ')
                for token in tokens:
                    token = token.strip()
                    if token == '':
                        continue
                    dic_src_tokens_tmp[token] = dic_src_tokens_tmp.get(token, 0) + 1

                tokens = tar.strip().split(' ')
                for token in tokens:
                    token = token.strip()
                    if token == '':
                        continue
                    dic_tar_tokens_tmp[token] = dic_tar_tokens_tmp.get(token, 0) + 1

        def _output_global_token(dic_tokens_tmp, dic_tokens, outobj, start_index):
            pq = queue.PriorityQueue()
            for token in dic_tokens_tmp:
                pq.put((-dic_tokens_tmp[token], token))
            cur_index = start_index
            while not pq.empty():
                freq, token = pq.get()
                dic_tokens[token] = cur_index
                cur_index += 1
                if len(dic_tokens) == self.MAX_VOCAB_SIZE:
                    break

            for key, idx in dic_tokens.items():
                write_line = '\t'.join([key, str(idx)])
                write_line += '\n'
                outobj.write(write_line)

        _output_global_token(dic_src_tokens_tmp, dic_src_tokens, src_obj, index)
        _output_global_token(dic_tar_tokens_tmp, dic_tar_tokens, tar_obj, index)

    def __get_train_data(self):
        print('generating train data...')
        de_obj = open(self.train_de_data_path)
        en_obj = open(self.train_en_data_path)

        index = 0
        for (de_line, en_line) in zip(de_obj, en_obj):
            if index % 1e5 == 0:
                print('current index: ', index)
            index += 1
            de_line = helper_fn.clear_string(de_line).strip()
            en_line = helper_fn.clear_string(en_line).strip()
            if de_line == '' or en_line == '':
                continue
            seq = []
            seq.append(str(index))
            seq.append(en_line)
            seq.append(de_line)
            write_line = '\t'.join(seq)
            write_line += '\n'
            self.dic_qa_objs['train'].write(write_line)

        de_obj.close()
        en_obj.close()

    def __get_valid_test_data(self, de_data_path, en_data_path, out_obj):
        de_obj = open(de_data_path)
        en_obj = open(en_data_path)
        print('generating valid and test data...')

        patten = '>(.*?)<'

        index = 0
        for (de_line, en_line) in zip(de_obj, en_obj):
            if index % 1e5 == 0:
                print('current index: ', index)

            index += 1
            m1 = re.search(patten, de_line.strip())
            m2 = re.search(patten, en_line.strip())
            if m1 == None or m2 == None:
                continue

            de_line = m1.group(1)
            en_line = m2.group(1)

            de_line = helper_fn.clear_string(de_line).strip()
            en_line = helper_fn.clear_string(en_line).strip()
            seq = []
            seq.append(str(index))
            seq.append(en_line)
            seq.append(de_line)
            write_line = '\t'.join(seq)
            write_line += '\n'
            out_obj.write(write_line)

        de_obj.close()
        en_obj.close()

    def __filter_all_files(self):
        print('start filtering the train, valid and test sets...')
        dic_token2index = dict()
        def _get_global_tokens():
            print('reading global tokens...')
            dic_t2i = dict()
            with open(self.tar_global_token_path) as f:
                for line in f:
                    elems = line.strip().split('\t')
                    token = elems[0].strip()
                    index = elems[1].strip()
                    dic_t2i[token] = index
            return dic_t2i

        # only keep the sentence that UNK tag less than 10%
        def _check_whether_kept(question, target):
            unk_index = dic_token2index[config.UNK_TOKEN]
            q_unk_count = 0
            question_tokens = question.strip().split(' ')
            for token in question_tokens:
                token = token.strip()
                if token == '':
                    continue
                idx = dic_token2index.get(token, unk_index)
                if idx == unk_index:
                    q_unk_count += 1

            a_unk_count = 0
            target_tokens = target.strip().split(' ')
            for token in target_tokens:
                token = token.strip()
                if token == '':
                    continue
                idx = dic_token2index.get(token, unk_index)
                if idx == unk_index:
                    a_unk_count += 1

            ques_rate = q_unk_count * 1.0 / len(question_tokens)
            tar_rate = a_unk_count * 1.0 / len(target_tokens)

            if ques_rate > self.QA_UNK_RATE or tar_rate > self.QA_UNK_RATE:
                return False
            return True 

        # filter data with UNK tag rate (less than 10% UNK tag)
        def _filter_data(qa_data_tmp, qa_outobj):
            with open(qa_data_tmp) as f:
                for line in f:
                    elems = line.strip().split('\t')
                    if len(elems) != 3:
                        continue
                    key = elems[0].strip()
                    ques = elems[1].strip()
                    res = elems[2].strip()
                    is_kept = _check_whether_kept(ques, res)
                    if is_kept == True:
                        write_line = '\t'.join([key, ques, res]) + '\n'
                        qa_outobj.write(write_line)

        train_qa_tmp = self.train_qa_data_path + '.tmp'
        if os.path.exists(train_qa_tmp):
            os.remove(train_qa_tmp)
        os.rename(self.train_qa_data_path, train_qa_tmp) 

        dic_token2index = _get_global_tokens()
        self.dic_qa_objs['train'] = open(self.train_qa_data_path, 'w')
        _filter_data(train_qa_tmp, self.dic_qa_objs['train'])
        self.dic_qa_objs['train'].close() 

    def get_de_en_data(self):
        self.__init_outobjs()
        self.__get_train_data()
        self.__get_valid_test_data(self.valid_de_data_path, self.valid_en_data_path, self.dic_qa_objs['valid'])
        self.__get_valid_test_data(self.test_de_data_path, self.test_en_data_path, self.dic_qa_objs['test'])
        self.__generate_global_token()
        self.__filter_all_files()
        self.__close_outobjs()

if __name__ == '__main__':
    ed = ExtractData(
                     config.train_de_data_path,
                     config.train_en_data_path, 
                     config.valid_de_data_path,
                     config.valid_en_data_path, 
                     config.test_de_data_path,
                     config.test_en_data_path, 
                     config.de_en_train_qa_path,
                     config.de_en_valid_qa_path,
                     config.de_en_test_qa_path,
                     config.de_en_src_global_token_path,
                     config.de_en_tar_global_token_path
                    )
    ed.get_de_en_data()

