# !/usr/bin/env python
import os, sys
from .tokenizers import *
from typing import Iterable, List, Optional

# generate checkpoint folder
def makedirs(fld):
    if not os.path.exists(fld):
        os.makedirs(fld)

def split_ex(sent):
    return [x.strip() for x in re.split('(\W+)?', sent) if x.strip()]

def get_stop_words(data_path):
    print('reading stop words...')
    dic_stop_words = dict()
    with open(data_path) as stopwords_file:
        for line in stopwords_file:
            dic_stop_words[line.strip()] = 1
    return dic_stop_words

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
    
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    
    return False

def clear_string(str_in): 
    return clean_str(str_in)

def pad_with_start_end(
               sample: Iterable[int],
               required_sequence_length: int,
               start_id: int,
               end_id: int,
               pad_id: int):
    
    # 2 means: <START> and <END>
    seq_length = required_sequence_length
    new_s = [start_id] + sample + [end_id]
    new_s += [pad_id] * (seq_length - len(new_s))
    return new_s

def pad_with_end(
               sample: Iterable[int],
               required_sequence_length: int,
               end_id: int,
               pad_id: int):
    
    seq_length = required_sequence_length
    new_s = sample + [end_id]
    new_s += [pad_id] * (seq_length - len(new_s))
    return new_s

def pad_with_start(
               sample: Iterable[int],
               required_sequence_length: int,
               start_id: int,
               pad_id: int):
    
    seq_length = required_sequence_length
    new_s = [start_id] + sample
    new_s += [pad_id] * (seq_length - len(new_s))
    return new_s

def pad_with_pad(
               sample: Iterable[int],
               required_sequence_length: int,
               pad_id: int):
    # keep the same length with the 'pad_with_start_end'
    seq_length = required_sequence_length
    new_s = sample + [pad_id] * (seq_length - len(sample))
    return new_s


if __name__ == '__main__':
    s = '<p> each of rankins congressional terms coincided with initiation of u . s . military intervention in each of the       two world wars . a lifelong pacifist and a supporter of non-interventionism , [ 3 ] she was one of 50 house members , along with 6 senato      rs , who opposed the war declaration of 1917 , and the only member of congress to vote against declaring war on japan after the attack on       pearl harbor in 1941 . [ 4 ] [ 5 ]'
    print(clear_string(s))
