import numpy as np
# noinspection PyPep8Naming
from keras import backend as K
#from keras.engine import Layer
from keras.layers import Layer
from keras.utils import get_custom_objects


class PGGens(Layer):
    """
    """
    def __init__(self, 
                 fact_number: int,
                 dropout: float = 0.0,
                 **kwargs):
        """
        :param fact_number: number of facts 
        :param kwargs: any extra arguments typical for a Keras layer,
          such as name, etc.
        """
        self.fact_number = fact_number
        self.dropout = dropout
        self.gen_number = 3 # for generation, src_attn, and fact_attn
        #self.gen_number = 1
        super().__init__(**kwargs)

    def get_config(self):
        config = super().get_config()
        config['fact_number'] = self.fact_number
        config['dropout'] = self.dropout
        return config

    def build(self, input_shape):
        if not (isinstance(input_shape, list) and len(input_shape) == 4):
            raise ValueError(
                'You must call this layer passing a list of 4 tensors')
        tar_len = input_shape[0][-2]
        d_model = input_shape[0][-1]

        self.W_dec_inp = self.add_weight(
            name='W_dec_inp', shape=(d_model, self.gen_number),
            initializer='glorot_normal', trainable=True)
        self.W_dec_self_attn = self.add_weight(
            name='W_dec_self_attn', shape=(d_model, self.gen_number),
            initializer='glorot_normal', trainable=True)
        self.W_enc_src = self.add_weight(
            name='W_enc_src', shape=(d_model, self.gen_number),
            initializer='glorot_normal', trainable=True)
        self.W_enc_fact = self.add_weight(
            name='W_enc_fact', shape=(self.fact_number, d_model, self.gen_number),
            initializer='glorot_normal', trainable=True)
        self.b = self.add_weight(
            name='b', shape=(self.gen_number,),
            initializer='zeros', trainable=True)

        return super().build(input_shape)

    def compute_output_shape(self, input_shape):
        dec_shape = input_shape[0]

        bs = dec_shape[0]
        tar_len = dec_shape[-2]
        return (bs, tar_len, self.gen_number)

    def apply_dropout_if_needed(self, _input, training=None):
        if 0.0 < self.dropout < 1.0:
            def dropped_softmax():
                return K.dropout(_input, self.dropout)

            return K.in_train_phase(dropped_softmax, _input,
                                    training=training)
        return _input

    def call(self, inputs, **kwargs):
        dec_input, dec_self_attn, src_attn_value, fact_attn_value = inputs

        dec_input_shape = K.shape(dec_input)
        bs = dec_input_shape[0]
        tar_len = dec_input_shape[-2]
        d_model = dec_input_shape[-1]

        p_gens = K.dot(K.reshape(dec_input, (-1, d_model)), self.W_dec_inp)
        p_gens += K.dot(K.reshape(dec_self_attn, (-1, d_model)), self.W_dec_self_attn)
        p_gens += K.dot(K.reshape(src_attn_value, (-1, d_model)), self.W_enc_src)

        fact_attn_value = K.permute_dimensions(fact_attn_value, [1, 0, 2, 3])
        for i in range(self.fact_number):
            p_gens += K.dot(K.reshape(fact_attn_value[i], (-1, d_model)), self.W_enc_fact[i])

        p_gens = K.bias_add(p_gens, self.b)
        p_gens = K.sigmoid(p_gens)
        p_gens = self.apply_dropout_if_needed(p_gens)
        p_gens = K.reshape(p_gens, (bs, tar_len, self.gen_number))
        p_gens = K.softmax(p_gens)
        return p_gens

get_custom_objects().update({
    'PGGens': PGGens,
})

