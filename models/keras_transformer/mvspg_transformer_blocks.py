"""
Contains implementation of the Transformer model described in papers
"Attention is all you need" (https://arxiv.org/abs/1706.03762) and
"Universal Transformer" (https://arxiv.org/abs/1807.03819)
"""
import math
from typing import Union, Callable, Optional

from keras.layers import Layer, Add, Dropout, Lambda, Reshape, Concatenate
from keras import initializers, activations
# noinspection PyPep8Naming
from keras import backend as K
from keras.utils import get_custom_objects

from models.keras_transformer.mvspg_attention import MultiHeadSelfAttention
from models.keras_transformer.mvspg_attention import MultiHeadAttention
from models.keras_transformer.mvspg_pointer import PGPointer, PGReverseContext
from models.keras_transformer.mvspg_gens import PGGens
from models.keras_transformer.mvspg_switch import PGSwitch, PGSwitchUpdate
from models.keras_transformer.mvspg_transition import TransformerTransition
from models.keras_transformer.normalization import LayerNormalization

class MVSPGTransformerEncoderBlock:
    """
    A pseudo-layer combining together all nuts and bolts to assemble
    a complete section of both the Transformer and the Universal Transformer
    models, following description from the "Universal Transformers" paper.
    Each such block is, essentially:

    - Multi-head self-attention (masked or unmasked, with attention dropout,
      but without input dropout)
    - Residual connection,
    - Dropout
    - Layer normalization
    - Transition function
    - Residual connection
    - Dropout
    - Layer normalization

    Also check TransformerACT class if you need support for ACT (Adaptive
    Computation Time).

    IMPORTANT: The older Transformer 2017 model ("Attention is all you need")
    uses slightly different order of operations. A quote from the paper:

        "We apply dropout [33] to the output of each sub-layer,
         before it is added to the sub-layer input and normalized"

    while the Universal Transformer paper puts dropout one step *after*
    the sub-layers's output was added to its input (Figure 4 in the paper).

    In this code the order from the Universal Transformer is used, as arguably
    more reasonable. You can use classical Transformer's (2017) way of
    connecting the pieces by passing vanilla_wiring=True to the constructor.
    """
    def __init__(self, name: str, num_heads: int, 
                 elem_number: int,
                 residual_dropout: float = 0, attention_dropout: float = 0,
                 activation: Optional[Union[str, Callable]] = 'relu',
                 vanilla_wiring=True):
        self.self_attention_layer = MultiHeadSelfAttention(
            num_heads, 
            elem_number,
            dropout=attention_dropout,
            name=f'{name}_self_attention')
        self.norm1_layer = LayerNormalization(name=f'{name}_norm1')
        self.dropout_layer = (
            Dropout(residual_dropout, name=f'{name}_dropout')
            if residual_dropout > 0
            else lambda x: x)
        self.norm2_layer = LayerNormalization(name=f'{name}_norm2')
        self.transition_layer = TransformerTransition(name=f'{name}_transition', activation=activation)
        self.add_layer = Add(name=f'{name}_add')
        self.vanilla_wiring = vanilla_wiring

    def __call__(self, inputs):
        _input, self_attn_mask = inputs
        print('in the encoder..........................')
        print(_input)
        output = self.self_attention_layer([_input, self_attn_mask])
        post_residual1 = (
            self.add_layer([_input, self.dropout_layer(output)]) # by default, self.vanilla_writing is False, so go else branch
            if self.vanilla_wiring
            else self.dropout_layer(self.add_layer([_input, output])))
        norm1_output = self.norm1_layer(post_residual1)
        print('norm1_output: ', norm1_output)

        output = self.transition_layer(norm1_output)
        post_residual2 = (
            self.add_layer([norm1_output, self.dropout_layer(output)])
            if self.vanilla_wiring
            else self.dropout_layer(
                self.add_layer([norm1_output, output])))
        output = self.norm2_layer(post_residual2)
        print('output: ', output)
        return output

class MVSPGTransformerDecoderBlock:
    """
    A pseudo-layer combining together all nuts and bolts to assemble
    a complete section of both the Transformer and the Universal Transformer
    models, following description from the "Universal Transformers" paper.
    Each such block is, essentially:

    - Multi-head self-attention (masked or unmasked, with attention dropout,
      but without input dropout)
    - Residual connection,
    - Dropout
    - Layer normalization
    - Multi-head attention (between dec input and enc output)
    - Residual connection,
    - Dropout
    - Layer normalization
    - Transition function
    - Residual connection
    - Dropout
    - Layer normalization

    Also check TransformerACT class if you need support for ACT (Adaptive
    Computation Time).

    IMPORTANT: The older Transformer 2017 model ("Attention is all you need")
    uses slightly different order of operations. A quote from the paper:

        "We apply dropout [33] to the output of each sub-layer,
         before it is added to the sub-layer input and normalized"

    while the Universal Transformer paper puts dropout one step *after*
    the sub-layers's output was added to its input (Figure 4 in the paper).

    In this code the order from the Universal Transformer is used, as arguably
    more reasonable. You can use classical Transformer's (2017) way of
    connecting the pieces by passing vanilla_wiring=True to the constructor.
    """
    def __init__(self, name: str, num_heads: int, 
                 residual_dropout: float = 0, attention_dropout: float = 0,
                 activation: Optional[Union[str, Callable]] = 'relu',
                 vanilla_wiring=True):
        print('This is in %s...' % name)
        self.self_attention_layer = MultiHeadSelfAttention(
            num_heads, 
            1,
            dropout=attention_dropout,
            name=f'{name}_self_attention')
        self.mutual_attention_layer = MultiHeadAttention(
            num_heads, 
            1,
            dropout=attention_dropout,
            name=f'{name}_mutual_attention')

        self.norm1_layer = LayerNormalization(name=f'{name}_norm1')
        self.norm2_layer = LayerNormalization(name=f'{name}_norm2')
        self.norm3_layer = LayerNormalization(name=f'{name}_norm3')

        self.dropout_layer = (
            Dropout(residual_dropout, name=f'{name}_dropout')
            if residual_dropout > 0
            else lambda x: x)
        self.transition_layer = TransformerTransition(name=f'{name}_transition', activation=activation)
        self.add_layer = Add(name=f'{name}_add')
        self.vanilla_wiring = vanilla_wiring

    def __call__(self, inputs):
        dec_input, self_attn_mask, \
        src_enc_output, mutual_tar_src_mask = inputs

        # self attention part
        output = self.self_attention_layer([dec_input, self_attn_mask])
        post_residual1 = (
            self.add_layer([dec_input, self.dropout_layer(output)])
            if self.vanilla_wiring
            else self.dropout_layer(self.add_layer([dec_input, output])))
        norm1_output = self.norm1_layer(post_residual1)

        output = self.mutual_attention_layer([src_enc_output, norm1_output, mutual_tar_src_mask])
        post_residual2 = (
            self.add_layer([norm1_output, self.dropout_layer(output)])
            if self.vanilla_wiring
            else self.dropout_layer(
                self.add_layer([norm1_output, output])))
        norm2_output = self.norm2_layer(post_residual2)

        output = self.transition_layer(norm2_output)
        post_residual3 = (
            self.add_layer([norm2_output, self.dropout_layer(output)])
            if self.vanilla_wiring
            else self.dropout_layer(
                self.add_layer([norm2_output, output])))
        final_output = self.norm3_layer(post_residual3)

        return final_output

class MVSPGTransformerDecoderLastBlock:
    """
    A pseudo-layer combining together all nuts and bolts to assemble
    a complete section of both the Transformer and the Universal Transformer
    models, following description from the "Universal Transformers" paper.
    Each such block is, essentially:

    - Multi-head self-attention (masked or unmasked, with attention dropout,
      but without input dropout)
    - Residual connection,
    - Dropout
    - Layer normalization
    - Multi-head attention (between dec input and enc output)
    - Residual connection,
    - Dropout
    - Layer normalization
    - Transition function
    - Residual connection
    - Dropout
    - Layer normalization

    Also check TransformerACT class if you need support for ACT (Adaptive
    Computation Time).

    IMPORTANT: The older Transformer 2017 model ("Attention is all you need")
    uses slightly different order of operations. A quote from the paper:

        "We apply dropout [33] to the output of each sub-layer,
         before it is added to the sub-layer input and normalized"

    while the Universal Transformer paper puts dropout one step *after*
    the sub-layers's output was added to its input (Figure 4 in the paper).

    In this code the order from the Universal Transformer is used, as arguably
    more reasonable. You can use classical Transformer's (2017) way of
    connecting the pieces by passing vanilla_wiring=True to the constructor.
    """
    def __init__(self, name: str, num_heads: int, 
                 fact_number: int,
                 fact_len: int,
                 tar_len: int,
                 src_len: int,
                 residual_dropout: float = 0, attention_dropout: float = 0,
                 activation: Optional[Union[str, Callable]] = 'relu',
                 vanilla_wiring=True):
        print('This is in %s...' % name)
        self.self_attention_layer = MultiHeadSelfAttention(
            num_heads, 
            1,
            dropout=attention_dropout,
            name=f'{name}_self_attention')
        self.mutual_attention_layer = MultiHeadAttention(
            num_heads, 
            1,
            dropout=attention_dropout,
            name=f'{name}_mutual_attention')

        self.pg_pointer_layer = PGPointer(
            fact_number,
            tar_len,
            dropout=attention_dropout,
            name=f'{name}_pg_pointer')

        self.pg_fact_context_layer = PGReverseContext(
            fact_number,
            tar_len,
            dropout=attention_dropout,
            name=f'{name}_pg_fact_dec_ctxt')

        self.pg_fact_switch_layer = PGSwitch(
            fact_number,
            fact_len, 
            dropout=attention_dropout,
            name=f'{name}_pg_fact_switch')

        self.pg_switch_update_layer = PGSwitchUpdate(
            tar_len, 
            name=f'{name}_pg_switch_update')

        self.pg_src_context_layer = PGReverseContext(
            1,
            tar_len,
            dropout=attention_dropout,
            name=f'{name}_pg_src_dec_ctxt')

        self.pg_src_switch_layer = PGSwitch(
            1,
            src_len, 
            dropout=attention_dropout,
            name=f'{name}_pg_src_switch')

        self.pg_gen_layer = PGGens(
            fact_number,
            dropout=attention_dropout,
            name=f'{name}_pg_gens')

        self.fact_number = fact_number
        self.fact_len = fact_len
        self.tar_len = tar_len
        self.norm1_layer = LayerNormalization(name=f'{name}_norm1')
        self.norm2_layer = LayerNormalization(name=f'{name}_norm2')
        self.norm3_layer = LayerNormalization(name=f'{name}_norm3')

        self.dropout_layer = (
            Dropout(residual_dropout, name=f'{name}_dropout')
            if residual_dropout > 0
            else lambda x: x)
        self.transition_layer = TransformerTransition(name=f'{name}_transition', activation=activation)
        self.add_layer = Add(name=f'{name}_add')
        self.vanilla_wiring = vanilla_wiring

    def __call__(self, inputs):
        dec_input, self_attn_mask, \
        src_enc_output, mutual_tar_src_mask, \
        fact_enc_output, mutual_tar_fact_mask, \
        mutual_src_dec_ctxt_mask, mutual_fact_dec_ctxt_mask = inputs

        #print('[[[start]]]')
        #print('dec_input: ', dec_input.shape)
        #print('src_enc_output: ', K.shape(src_enc_output))
        #print('mutual_tar_src_mask: ', mutual_tar_src_mask)
        #print('fact_enc_output: ', fact_enc_output)
        #print('mutual_tar_fact_mask: ', mutual_tar_fact_mask)
        #print('[[[end start]]]')

        # self attention part
        output = self.self_attention_layer([dec_input, self_attn_mask])
        post_residual1 = (
            self.add_layer([dec_input, self.dropout_layer(output)])
            if self.vanilla_wiring
            else self.dropout_layer(self.add_layer([dec_input, output])))
        norm1_output = self.norm1_layer(post_residual1)

        src_context_value = self.pg_src_context_layer([norm1_output, src_enc_output, mutual_src_dec_ctxt_mask])
        src_soft_switches = self.pg_src_switch_layer(src_context_value)

        fact_context_value = self.pg_fact_context_layer([norm1_output, fact_enc_output, mutual_fact_dec_ctxt_mask])
        fact_soft_switches = self.pg_fact_switch_layer(fact_context_value)

        src_attn_value, src_attn_dist, \
        fact_attn_value, fact_attn_dist = self.pg_pointer_layer([norm1_output, 
                src_enc_output,
                mutual_tar_src_mask,
                fact_enc_output,
                mutual_tar_fact_mask,
                src_soft_switches,
                fact_soft_switches,
        ])

        src_attn_dist = self.pg_switch_update_layer([src_attn_dist, src_soft_switches])
        fact_attn_dist = self.pg_switch_update_layer([fact_attn_dist, fact_soft_switches])
        p_gens = self.pg_gen_layer([dec_input, norm1_output, src_attn_value, fact_attn_value])

        output = self.mutual_attention_layer([src_enc_output, norm1_output, mutual_tar_src_mask])
        post_residual2 = (
            self.add_layer([norm1_output, self.dropout_layer(output)])
            if self.vanilla_wiring
            else self.dropout_layer(
                self.add_layer([norm1_output, output])))
        norm2_output = self.norm2_layer(post_residual2)

        output = self.transition_layer(norm2_output)
        post_residual3 = (
            self.add_layer([norm2_output, self.dropout_layer(output)])
            if self.vanilla_wiring
            else self.dropout_layer(
                self.add_layer([norm2_output, output])))
        final_output = self.norm3_layer(post_residual3)

        return [final_output, src_attn_dist, fact_attn_dist, p_gens]


get_custom_objects().update({
    'MVSPGTransformerEncoderBlock': MVSPGTransformerEncoderBlock,
    'MVSPGTransformerDecoderBlock': MVSPGTransformerDecoderBlock,
    'MVSPGTransformerDecoderLastBlock': MVSPGTransformerDecoderLastBlock,
})


