import numpy as np
from keras import backend as K
from keras.layers import Layer, Add
from keras.utils import get_custom_objects

class PGWordPrediction(Layer):
    """
    """
    def __init__(self, 
                 src_len,
                 tar_len,
                 fact_number,
                 fact_len,
                 vocab_size,
                 **kwargs):
        """
        :param kwargs: any extra arguments typical for a Keras layer,
          such as name, etc.
        """
        self.src_len = src_len
        self.tar_len = tar_len
        self.fact_number = fact_number
        self.fact_len = fact_len
        self.vocab_size = vocab_size
        self.extra_oov_size = 10000
        super().__init__(**kwargs)

    def get_config(self):
        config = super().get_config()
        config['src_len'] = self.src_len
        config['tar_len'] = self.tar_len
        config['fact_number'] = self.fact_number
        config['fact_len'] = self.fact_len
        config['vocab_size'] = self.vocab_size
        return config

    def build(self, input_shape):
        if not (isinstance(input_shape, list) and len(input_shape) == 6):
            raise ValueError(
                'You must call this layer passing a list of 6 tensors')
        return super().build(input_shape)

    def compute_output_shape(self, input_shape):
        word_prediction_shape = input_shape[0]
        return (word_prediction_shape[0], word_prediction_shape[1], word_prediction_shape[2] + self.extra_oov_size)

    def call(self, inputs, **kwargs):
        print('in pg word prediction...............')
        gen_word_pred, src_attn_dist, fact_attn_dist, p_gens, \
        inp_src_extend_oov, inp_facts_extend_oov = inputs

        # shape: (bs, tar_len, vocab_size)
        final_shape = K.shape(gen_word_pred)
        batch_size = final_shape[0]

        max_oov_len = self.extra_oov_size
        ext_vocab_size = self.vocab_size + max_oov_len

        # used for 3 gens
        p_gens = K.permute_dimensions(p_gens, [2, 0, 1])
        gen_generate = p_gens[0]
        gen_src_attn = p_gens[1]
        gen_fact_attn = p_gens[2]
        gen_generate = K.expand_dims(gen_generate, axis=-1)
        gen_src_attn = K.expand_dims(gen_src_attn, axis=-1)
        gen_fact_attn = K.expand_dims(gen_fact_attn, axis=-1)

        # incorporate gens to generation, question and fact attentions
        gen_word_pred = gen_generate * gen_word_pred
        src_attn_dist = K.reshape(src_attn_dist, (-1, self.tar_len, self.src_len))
        src_attn_dist = gen_src_attn * src_attn_dist
        gen_fact_attn = K.expand_dims(gen_fact_attn, axis=1)
        gen_fact_attn = K.repeat_elements(gen_fact_attn, self.fact_number, axis=1)
        fact_attn_dist = gen_fact_attn * fact_attn_dist

        # extend generative word distribution
        extra_zeros = K.zeros((batch_size, max_oov_len))
        gen_word_pred = K.permute_dimensions(gen_word_pred, [1, 0, 2])
        gen_word_pred_ext = []
        for i in range(self.tar_len):
            gen_word_step = gen_word_pred[i]
            gen_word_step_ext = K.concatenate([gen_word_step, extra_zeros], axis=1) 
            gen_word_pred_ext.append(gen_word_step_ext)
        gen_word_pred_ext = K.stack(gen_word_pred_ext)
        print('gen_word_pred_ext1: ', gen_word_pred_ext)
        gen_word_pred_ext = K.permute_dimensions(gen_word_pred_ext, [1, 0, 2])
        print('gen_word_pred_ext2: ', gen_word_pred_ext)

        # prepare src attention dist
        pre_src = K.tf.range(0, limit=batch_size) # shape (batch_size)
        pre_src = K.expand_dims(pre_src, 1) # shape (batch_size, 1)
        pre_src = K.tile(pre_src, [1, self.src_len]) # shape (batch_size, src_attn_len)
        src_indices = K.stack((pre_src, inp_src_extend_oov), axis=2) # shape (batch_size, enc_t, 2)                                         

        # prepare fact attention dist
        pre_fact = K.tf.range(0, limit=batch_size) # shape (batch_size)
        pre_fact = K.expand_dims(pre_fact, 1) # shape (batch_size, 1)
        pre_fact = K.tile(pre_fact, [1, self.fact_len]) # shape (batch_size, fact_attn_len)

        avg_sum = [] 
        avg = []
        for i in range(self.tar_len):
            avg_sum.append(K.zeros((batch_size, ext_vocab_size)))
            avg.append(K.zeros((batch_size, ext_vocab_size)))
        shape = [batch_size, ext_vocab_size]

        # sum question attention
        src_attn_dist = K.permute_dimensions(src_attn_dist, [1, 0, 2]) # (tar_len, bs, src_len)
        for i in range(self.tar_len): 
            copy_dist = src_attn_dist[i] 
            tmp = K.tf.scatter_nd(src_indices, copy_dist, shape)
            tmp_ones = K.cast(K.greater(tmp, 0), dtype=K.floatx())
            avg_sum[i] = Add()([avg_sum[i], tmp_ones])
            avg[i] = Add()([avg[i], tmp])

        # sum facts attention
        inp_facts = K.permute_dimensions(inp_facts_extend_oov, [1, 0, 2])
        print('-' * 100)
        print('inp_facts: ', inp_facts)
        fact_attn_dist = K.permute_dimensions(fact_attn_dist, [1, 2, 0, 3])
        for i in range(self.fact_number):
            inp_fact = inp_facts[i]
            print('inp_fact: ', inp_fact)
            fact_indices = K.stack((pre_fact, inp_fact), axis=2) # shape (batch_size, enc_t, 2)                                         
            for j in range(self.tar_len): 
                copy_dist = fact_attn_dist[i][j] 
                tmp = K.tf.scatter_nd(fact_indices, copy_dist, shape)
                tmp_ones = K.cast(K.greater(tmp, 0), dtype=K.floatx())
                avg_sum[j] = Add()([avg_sum[j], tmp_ones])
                avg[j] = Add()([avg[j], tmp])

        avg_sum = K.stack(avg_sum)
        avg = K.stack(avg)
        avg_sum += K.constant(1e-9) # give a very small number in case of 0 to be demoninator
        avg = avg / avg_sum
        avg = K.permute_dimensions(avg, [1, 0, 2])

        final_word_prediction = gen_word_pred_ext + avg
        print('gen_word_pred_ext: ', gen_word_pred_ext )
        print('avg: ', avg)
        print('final_word_prediction: ', final_word_prediction )

        return final_word_prediction

get_custom_objects().update({
    'PGWordPrediction': PGWordPrediction,
})

