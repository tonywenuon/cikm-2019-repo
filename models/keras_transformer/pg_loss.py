import numpy as np
# noinspection PyPep8Naming
from keras import backend as K
#from keras.engine import Layer
from keras.layers import Layer
from keras.utils import get_custom_objects


class PGLoss(Layer):
    """
    """
    def __init__(self, 
                 tar_len: int,
                 **kwargs):
        """
        :param fact_number: number of facts 
        :param kwargs: any extra arguments typical for a Keras layer,
          such as name, etc.
        """
        self.tar_len = tar_len
        super().__init__(**kwargs)

    def get_config(self):
        config = super().get_config()
        config['tar_len'] = self.tar_len
        return config

    def _mask_and_avg(self, values, padding_mask):
        """Applies mask to values then returns overall average (a scalar)
        
        Args:
          values: a list length max_dec_steps containing arrays shape (batch_size).
          padding_mask: tensor shape (batch_size, max_dec_steps) containing 1s and 0s.
        
        Returns:
          a scalar
        """
        
        dec_lens = K.sum(padding_mask, axis=1) # shape batch_size. float32
        dec_lens += K.constant(1e-7)
        print('dec_lens: ', dec_lens)
        values_per_step = values * padding_mask
        print('values_per_step1: ', values_per_step)
        values_per_step = K.sum(values_per_step, axis=1)
        print('values_per_step2: ', values_per_step)
        values_per_step = values_per_step / dec_lens
        print('values_per_step3: ', values_per_step)
        overall_avg = K.mean(values_per_step)
        print('overall_avg: ', overall_avg)

        return overall_avg # overall average

    def compute_output_shape(self, input_shape):
        return (1,)
        
    def call(self, inputs, **kwargs):
        word_predictions = inputs[0]
        inp_tar = inputs[1]
        tar_padding_mask = inputs[2]

        tar_shape = K.shape(inp_tar)
        bs = tar_shape[0]

        final_dists = K.permute_dimensions(word_predictions, [1, 0, 2])
        targets = inp_tar

        loss_per_step = [] # will be list length max_dec_steps containing shape (batch_size)
        batch_nums = K.tf.range(0, limit=bs) # shape (batch_size)
        for dec_step in range(self.tar_len):
            dist = final_dists[dec_step]
            target = targets[:, dec_step]
            indices = K.stack((batch_nums, target), axis=1) # shape (batch_size, 2)
            gold_probs = K.tf.gather_nd(dist, indices) # shape (batch_size). prob of correct words on this step                       
            #gold_probs += K.constant(1e-9)
            losses = -K.log(gold_probs)
            loss_per_step.append(losses)

        loss_per_step = K.stack(loss_per_step)
        loss_per_step = K.permute_dimensions(loss_per_step, [1, 0])
        print('tar_padding_mask: ', tar_padding_mask)
        print('loss_per_step: ', loss_per_step)
        # Apply dec_padding_mask and get loss
        loss = self._mask_and_avg(loss_per_step, tar_padding_mask)

        self.add_loss(loss)
        return loss
        #return word_predictions

get_custom_objects().update({
    'PGLoss': PGLoss,
})

