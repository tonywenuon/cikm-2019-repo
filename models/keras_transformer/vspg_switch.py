import numpy as np
# noinspection PyPep8Naming
from keras import backend as K
from keras.layers import Layer, Dense
from keras.utils import get_custom_objects


class PGSwitch(Layer):
    """
    """
    def __init__(self, 
                 elem_number: int,
                 elem_len: int,
                 dropout: float = 0.0,
                 **kwargs):
        """
        :param elem_number: number of element to be switched
        :param kwargs: any extra arguments typical for a Keras layer,
          such as name, etc.
        """
        self.elem_number = elem_number
        self.elem_len = elem_len
        self.dropout = dropout
        super().__init__(**kwargs)

    def get_config(self):
        config = super().get_config()
        config['elem_number'] = self.elem_number
        config['elem_len'] = self.elem_len
        config['dropout'] = self.dropout
        return config

    def build(self, input_shape):
        d_model = input_shape[-1]

        self.W_ctxt = self.add_weight(
            name='W_dec_inp', shape=(self.elem_number, self.elem_len, d_model, d_model),
            initializer='glorot_normal', trainable=True)

        self.b = self.add_weight(
            name='b', shape=(self.elem_number, self.elem_len, d_model, ),
            initializer='zeros', trainable=True)

        return super().build(input_shape)

    def compute_output_shape(self, input_shape):
        dec_shape = input_shape

        bs = dec_shape[0]
        tar_len = dec_shape[-2]
        return (bs, tar_len, 1)

    def apply_dropout_if_needed(self, _input, training=None):
        if 0.0 < self.dropout < 1.0:
            def dropped_softmax():
                return K.dropout(_input, self.dropout)
            return K.in_train_phase(dropped_softmax, _input,
                                       training=training)
        return _input

    def call(self, inputs, **kwargs):
        # 1, shape: (bs, elem_num, elem_len, d_model)
        reverse_ctxt_value = inputs

        ctxt_shape = K.shape(reverse_ctxt_value)
        d_model = ctxt_shape[-1]

        switches = []
        reverse_ctxt_value = K.permute_dimensions(reverse_ctxt_value, [1, 2, 0, 3])
        for i in range(self.elem_number):
            each_elem_switches = []
            for j in range(self.elem_len):
                switch = K.dot(K.reshape(reverse_ctxt_value[i][j], (-1, d_model)), self.W_ctxt[i][j])
                switch = K.bias_add(switch, self.b[i][j])
                each_elem_switches.append(switch)
            each_elem_switches = K.stack(each_elem_switches ) 
            switches.append(each_elem_switches)
        switches = K.stack(switches)
        switches = Dense(1)(switches)
        #switches = K.sigmoid(switches)
        switches = K.hard_sigmoid(switches)
        switches = self.apply_dropout_if_needed(switches)

        soft_switches = K.permute_dimensions(switches, [2, 0, 1, 3])

        return soft_switches

class PGSwitchUpdate(Layer):
    """
    """
    def __init__(self, 
                 tar_len: int,
                 dropout: float = 0.0,
                 **kwargs):
        """
        :param tar_len: target length
        :param kwargs: any extra arguments typical for a Keras layer,
          such as name, etc.
        """
        self.tar_len = tar_len
        super().__init__(**kwargs)

    def get_config(self):
        config = super().get_config()
        config['tar_len'] = self.tar_len
        return config

    def build(self, input_shape):
        if not (isinstance(input_shape, list) and len(input_shape) == 2):
            raise ValueError(
                'You must call this layer passing a list of 2 tensors')
        return super().build(input_shape)

    def compute_output_shape(self, input_shape):
        return input_shape[0]

    def call(self, inputs, **kwargs):
        # 1, shape: (bs, elem_num, tar_len, elem_len)
        # 2, shape: (bs, elem_num, elem_len, 1)
        attn_dist, soft_switches = inputs

        soft_switches = K.repeat_elements(soft_switches, self.tar_len, axis=-1)
        soft_switches = K.permute_dimensions(soft_switches, [0, 1, 3, 2])
        attn_dist = attn_dist * soft_switches

        return attn_dist


get_custom_objects().update({
    'PGSwitch': PGSwitch,
    'PGSwitchUpdate': PGSwitchUpdate,
})

