import numpy as np
# noinspection PyPep8Naming
from keras import backend as K
#from keras.engine import Layer
from keras.layers import Layer
from keras.utils import get_custom_objects


class _BasePGPointer(Layer):
    """
    """
    def __init__(self, 
                 fact_number: int,
                 tar_len: int,
                 dropout: float = 0.0,
                 **kwargs):
        """
        :param fact_number: number of facts 
        :param dropout: dropout that should be applied to the attention
          (after the softmax).
        :param kwargs: any extra arguments typical for a Keras layer,
          such as name, etc.
        """
        self.fact_number = fact_number
        self.tar_len = tar_len
        self.dropout = dropout
        super().__init__(**kwargs)

    def get_config(self):
        config = super().get_config()
        config['fact_number'] = self.fact_number
        config['tar_len'] = self.tar_len
        config['dropout'] = self.dropout
        return config

    # noinspection PyAttributeOutsideInit
    def build_output_params(self, d_model):
        self.output_weights = self.add_weight(
            name='output_weights',
            shape=(d_model, d_model),
            initializer='glorot_normal',
            trainable=True)

    def attention(self, pre_dec, pre_enc, \
        attn_mask, enc_num, out_seq_len, d_model, \
        soft_switches=None, training=None):

        """
        :param pre_dec: (bs, 1, tar_len, d_model)
        :param pre_enc: (bs, 1, src_len, d_model) or (bs, fact_number, fact_len, d_model)
        :param attn_mask: mask between target and question
        :param enc_num: 1 or fact_number
        :param out_seq_len: the length of the output sequence
        :param d_model: dimensionality of the model (by the paper)
        :param training: Passed by Keras. Should not be defined manually.
          Optional scalar tensor indicating if we're in training
          or inference phase.
        """
        #print('in attention function.......................')
        #print('pre_dec: ', pre_dec)
        #print('pre_enc: ', pre_enc)

        q = pre_dec
        v = pre_enc
        k_t = K.permute_dimensions(pre_enc, [0, 1, 3, 2])

        # for further matrix multiplication
        sqrt_d = K.sqrt(K.cast(d_model, dtype=K.floatx()))
        q_shape = K.shape(q)
        v_shape = K.shape(v)
        k_t_shape = K.shape(k_t)

        # before performing batch_dot all tensors are being converted to 3D
        reshape1 = K.reshape(q, (-1, q_shape[-2], q_shape[-1]))
        reshape2 = K.reshape(k_t, (-1, k_t_shape[-2], k_t_shape[-1]))
        attn_dist = self.apply_dropout_if_needed(
            self.mask_attention(
                # core scaled dot product
                K.batch_dot( 
                    reshape1, 
                    reshape2)
                / sqrt_d, attn_mask, soft_switches),
            training=training)

        attn_values = K.reshape(
            K.batch_dot(attn_dist, K.reshape(v, (-1, v_shape[-2], v_shape[-1]))),
            (-1, enc_num, q_shape[-2], q_shape[-1]))

        attn_values_merged = K.reshape(attn_values, (-1, d_model))
        attn_out = K.reshape(
            K.dot(attn_values_merged, self.output_weights),
            (-1, enc_num, out_seq_len, d_model))
        attn_dist_shape = K.shape(attn_dist)
        attn_dist = K.reshape(
            attn_dist,
            (-1, enc_num, attn_dist_shape[-2], attn_dist_shape[-1]))

        return attn_out, attn_dist

    def apply_dropout_if_needed(self, attention_softmax, training=None):
        if 0.0 < self.dropout < 1.0:
            def dropped_softmax():
                return K.dropout(attention_softmax, self.dropout)

            return K.in_train_phase(dropped_softmax, attention_softmax,
                                    training=training)
        return attention_softmax

    def mask_attention(self, dot_product, attn_mask, soft_switches=None, training=None):
        #print('in %s...' % self.name)
        #print('dot_product1: ', dot_product)
        #print('attn_mask1: ', attn_mask)
        dot_shape = K.shape(dot_product)
        q_len, k_len = dot_shape[-2], dot_shape[-1]
        mask_shape = K.shape(attn_mask)
        mask_q_len, mask_k_len = mask_shape[-2], mask_shape[-1]

        attn_mask = K.reshape(attn_mask, (-1, mask_q_len, mask_k_len))
        attn_mask = K.cast(attn_mask, dtype=K.floatx()) 

        # masking pad-id
        dot_product = dot_product * attn_mask
        if soft_switches is not None:
            # switch of some of the elements
            print('soft_switches1: ', soft_switches)
            soft_switches = K.repeat_elements(soft_switches, self.tar_len, axis=-1)
            # (bs, elem_num, tar_len, elem_len)
            soft_switches = K.permute_dimensions(soft_switches, [0, 1, 3, 2])
            print('soft_switches2: ', soft_switches)
            switch_shape = K.shape(soft_switches)
            switch_q_len, switch_k_len = switch_shape[-2], switch_shape[-1]
            soft_switches = K.reshape(soft_switches, (-1, switch_q_len, switch_k_len))
            print('soft_switches3: ', soft_switches)
            dot_product = dot_product * soft_switches
            print('dot_product1: ', dot_product)

        print('dot_product2: ', dot_product)
        close_to_negative_inf = K.constant(-1e9, dtype=K.floatx())
        negative_inf_matrix = 1 - attn_mask
        negative_inf_matrix *= close_to_negative_inf
        dot_product += negative_inf_matrix
        
        dot_product = K.softmax(dot_product)
        # after softmax, re-calc because if all elements are 0, 
        # softmax will give average weight, but they should be 0.
        dot_product = dot_product * attn_mask

        return dot_product
            
class PGPointer(_BasePGPointer):
    """
    """

    def build(self, input_shape):
        if not (isinstance(input_shape, list) and len(input_shape) == 7):
            raise ValueError(
                'You must call this layer passing a list of 7 tensors')
        d_model = input_shape[0][-1]

        self.W_dec= self.add_weight(
            name='W_dec', shape=(d_model, d_model),
            initializer='glorot_normal', trainable=True)
        self.W_src= self.add_weight(
            name='W_src', shape=(d_model, d_model),
            initializer='glorot_normal', trainable=True)
        self.W_fact= self.add_weight(
            name='W_fact', shape=(d_model, d_model),
            initializer='glorot_normal', trainable=True)
        self.build_output_params(d_model)

        return super().build(input_shape)

    def compute_output_shape(self, input_shape):
        dec_shape = input_shape[0]
        src_enc_shape = input_shape[1]
        fact_enc_shape = input_shape[3]

        bs = dec_shape[0]
        tar_num = dec_shape[1]
        tar_len = dec_shape[-2]
        d_model = dec_shape[-1]

        src_len = src_enc_shape[-2]
        fact_num = fact_enc_shape[1]
        fact_len = fact_enc_shape[-2]

        return [(bs, tar_num, tar_len, d_model), (bs, tar_num, tar_len, src_len), \
            (bs, fact_num, tar_len, d_model), (bs, fact_num, tar_len, fact_len)]

    def call(self, inputs, **kwargs):
        dec_output, \
        src_enc_output, mutual_tar_src_mask, \
        fact_enc_output, mutual_tar_fact_mask, \
        src_soft_switches, fact_soft_switches = inputs

        dec_shape = K.shape(dec_output)
        src_enc_shape = K.shape(src_enc_output)
        fact_enc_shape = K.shape(fact_enc_output)

        tar_len, d_model = dec_shape[-2], dec_shape[-1]
        src_enc_num, src_len = src_enc_shape[1], src_enc_shape[-2]
        fact_enc_num, fact_len = fact_enc_shape[1], fact_enc_shape[-2]

        dec_dot = K.dot(K.reshape(dec_output, [-1, d_model]), self.W_dec)
        src_enc_dot = K.dot(K.reshape(src_enc_output, [-1, d_model]), self.W_src)
        fact_enc_dot = K.dot(K.reshape(fact_enc_output, [-1, d_model]), self.W_fact)

        # mutual attention with src
        pre_dec_src = K.reshape(dec_dot, (-1, 1, tar_len, d_model))
        # mutual attention with fact
        pre_dec_fact = K.repeat_elements(pre_dec_src, self.fact_number, axis = 1) 

        # src from encoder
        pre_src_enc = K.reshape(src_enc_dot, (-1, 1, src_len, d_model))
        # fact from encoder
        pre_fact_enc = K.reshape(fact_enc_dot, (-1, self.fact_number, fact_len, d_model))

        src_attn_value, src_attn_dist = self.attention(pre_dec_src, pre_src_enc, \
            mutual_tar_src_mask, src_enc_num, tar_len, d_model, src_soft_switches)

        fact_attn_value, fact_attn_dist = self.attention(pre_dec_fact, pre_fact_enc, \
            mutual_tar_fact_mask, fact_enc_num, tar_len, d_model, fact_soft_switches)

        return [src_attn_value, src_attn_dist, fact_attn_value, fact_attn_dist]

class PGReverseContext(_BasePGPointer):
    """
    Different from traditional context, which calculates context with each encoder element,
    this context is calculated by enc_element with each decoder word
    """

    def build(self, input_shape):
        if not (isinstance(input_shape, list) and len(input_shape) == 3):
            raise ValueError(
                'You must call this layer passing a list of 3 tensors')
        d_model = input_shape[0][-1]

        self.W_dec= self.add_weight(
            name='W_dec', shape=(d_model, d_model),
            initializer='glorot_normal', trainable=True)
        self.W_enc= self.add_weight(
            name='W_enc', shape=(d_model, d_model),
            initializer='glorot_normal', trainable=True)
        self.build_output_params(d_model)

        return super().build(input_shape)

    def compute_output_shape(self, input_shape):
        enc_shape = input_shape[1]
        return enc_shape

    def call(self, inputs, **kwargs):
        dec_output, enc_output, mutual_tar_mask = inputs
        elem_number = self.fact_number
        if elem_number > 1:
            mutual_tar_mask = K.repeat_elements(mutual_tar_mask, elem_number, axis=1)
        print('mutual_tar_mask: ', mutual_tar_mask)

        dec_shape = K.shape(dec_output)
        enc_shape = K.shape(enc_output)

        d_model = dec_shape[-1]
        enc_len = enc_shape[-2]

        dec_dot = K.dot(K.reshape(dec_output, [-1, d_model]), self.W_dec)
        enc_dot = K.dot(K.reshape(enc_output, [-1, d_model]), self.W_enc)

        # decoder only have one element for rank 1
        pre_dec = K.reshape(dec_dot, (-1, 1, self.tar_len, d_model))
        if elem_number > 1:
            pre_dec = K.repeat_elements(pre_dec, elem_number, axis = 1) 

        pre_enc = K.reshape(enc_dot, (-1, elem_number, enc_len, d_model))

        attn_value, _ = self.attention(pre_enc, pre_dec, \
            mutual_tar_mask, elem_number, enc_len, d_model)

        return attn_value


get_custom_objects().update({
    'PGPointer': PGPointer,
    'PGReverseContext': PGReverseContext,
})

