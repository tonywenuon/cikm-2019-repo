import keras
from keras import regularizers
from keras.models import Model
# noinspection PyPep8Naming
from keras import backend as K
from keras.utils import get_custom_objects
from keras.layers import Input, Softmax, Embedding, Add, Lambda, Dense
from keras.layers import RepeatVector, Layer, Concatenate, Reshape

from keras_pos_embd import TrigPosEmbedding
from models.keras_transformer.extras import ReusableEmbedding, TiedOutputEmbedding
from models.keras_transformer.masks import PaddingMaskLayer, SequenceMaskLayer, PGPaddingMaskLayer
from models.keras_transformer.position import TransformerCoordinateEmbedding, SrcFactTransformerCoordinateEmbedding
from models.keras_transformer.transformer_blocks import TransformerACT
from models.keras_transformer.rear_vspg_transformer_blocks import VSPGTransformerEncoderBlock, VSPGTransformerDecoderBlock
from models.keras_transformer.vspg_word_prediction import PGWordPrediction
from models.keras_transformer.pg_obtain_layer import PGGetLayer
from models.keras_transformer.pg_loss import PGLoss

class VSPGTransformerModel:
    def __init__(self, args, 
                 transformer_dropout: float = 0.05,
                 embedding_dropout: float = 0.05,
                 l2_reg_penalty: float = 1e-4,
                 use_same_embedding = True,
                 use_vanilla_transformer = True,
                 ):
        self.args = args
        self.transformer_dropout = transformer_dropout 
        self.embedding_dropout = embedding_dropout

        # prepare layers
        l2_regularizer = (regularizers.l2(l2_reg_penalty) if l2_reg_penalty else None)
        if use_same_embedding:
            self.encoder_embedding_layer = self.decoder_embedding_layer = ReusableEmbedding(
                self.args.vocab_size, self.args.embedding_dim,
                name='embeddings',
                # Regularization is based on paper "A Comparative Study on
                # Regularization Strategies for Embedding-based Neural Networks"
                # https://arxiv.org/pdf/1508.03721.pdf
                embeddings_regularizer=l2_regularizer)
        else:
            self.encoder_embedding_layer = ReusableEmbedding(
                self.args.vocab_size, self.args.embedding_dim,
                name='encoder_embeddings',
                embeddings_regularizer=l2_regularizer)
            self.decoder_embedding_layer = ReusableEmbedding(
                self.args.vocab_size, self.args.embedding_dim,
                name='decoder_embeddings',
                embeddings_regularizer=l2_regularizer)

        self.output_layer = TiedOutputEmbedding(
            projection_dropout=self.embedding_dropout,
            scaled_attention=True,
            projection_regularizer=l2_regularizer,
            name='gen_word_pred_logits')
        self.output_softmax_layer = Softmax(name='word_pred_softmax')
        self.pg_get_src_dist_layer = PGGetLayer(name='src_attn_dist')
        self.pg_loss_layer = PGLoss(tar_len=self.args.tar_seq_length, name='final_loss')

        self.pg_word_predict_layer = PGWordPrediction(
            src_len=self.args.src_seq_length,
            tar_len=self.args.tar_seq_length,
            fact_number=self.args.fact_number,
            fact_len=self.args.src_seq_length,
            vocab_size=self.args.vocab_size,
            name='pg_word_pred_logits')

        self.encoder_coord_embedding_layer = SrcFactTransformerCoordinateEmbedding(
            max_sequence_length=self.args.src_seq_length,
            max_transformer_depth=1 if use_vanilla_transformer else self.args.transformer_depth,
            max_src_facts_depth=self.args.fact_number+1,
            name='encoder_coordinate_embedding')
        self.decoder_coord_embedding_layer = TransformerCoordinateEmbedding(
            max_sequence_length=self.args.tar_seq_length,
            max_transformer_depth=1 if use_vanilla_transformer else self.args.transformer_depth,
            name='decoder_coordinate_embedding')

    def __get_encoder(self, input_layer, _name):
        print('This is in Encoder...')
        self_attn_mask = PaddingMaskLayer(name='encoder_%s_self_mask'%_name, src_len=self.args.src_seq_length,
                                          pad_id=self.pad_id)(input_layer)

        elem_number = 1
        sf_step = 0
        if _name == 'fact':
            elem_number = self.args.fact_number
            sf_step = 1

        next_step_input, _ = self.encoder_embedding_layer(input_layer)
        next_step_input = self.encoder_coord_embedding_layer(next_step_input, step=0, src_fact_step=sf_step)
        for i in range(self.args.transformer_depth):
            encoder_block = VSPGTransformerEncoderBlock(
                    name='%s_transformer_encoder'%_name + str(i), 
                    num_heads=self.args.num_heads,
                    elem_number=elem_number,
                    residual_dropout=self.transformer_dropout,
                    attention_dropout=self.transformer_dropout,
                    activation='relu',
                    vanilla_wiring=True) # use vanilla Transformer instead of Universal Transformer
            next_step_input = encoder_block([next_step_input, self_attn_mask])
        return next_step_input

    def __get_decoder(self, input_layer, 
                      src_encoder_output, mutual_tar_src_mask,
                      fact_encoder_output, mutual_tar_fact_mask,
                      mutual_src_dec_ctxt_mask,
                      mutual_fact_dec_ctxt_mask,
                     ):
        print('This is in Decoder...')
        self_padding_mask = PaddingMaskLayer(name='decoder_self_padding_mask', src_len=self.args.tar_seq_length,
                                             pad_id=self.pad_id)(input_layer)
        seq_mask = SequenceMaskLayer()(input_layer)
        self_attn_mask = Add()([self_padding_mask, seq_mask])
        # greater than 1, means not be padded in both self_padding_mask and seq_mask
        self_attn_mask = Lambda(lambda x: K.cast(K.greater((x), 1), dtype='int32'), name='add_padding_seq_mask')(self_attn_mask)

        next_step_input, self.decoder_embedding_matrix = self.decoder_embedding_layer(input_layer)
        next_step_input = self.decoder_coord_embedding_layer(next_step_input, step=0)
        for i in range(self.args.transformer_depth):
            decoder_block = VSPGTransformerDecoderBlock(
                    name='transformer_decoder' + str(i), 
                    num_heads=self.args.num_heads,
                    fact_number=self.args.fact_number,
                    fact_len=self.args.src_seq_length,
                    tar_len=self.args.tar_seq_length,
                    src_len=self.args.src_seq_length,
                    residual_dropout=self.transformer_dropout,
                    attention_dropout=self.transformer_dropout,
                    activation='relu',
                    vanilla_wiring=True) # use vanilla Transformer instead of Universal Transformer
            next_step_input, src_attn_dist, fact_attn_dist, p_gens = decoder_block([
                next_step_input, self_attn_mask, \
                src_encoder_output, mutual_tar_src_mask, \
                fact_encoder_output, mutual_tar_fact_mask, \
                mutual_src_dec_ctxt_mask, mutual_fact_dec_ctxt_mask
            ])
        return next_step_input, src_attn_dist, fact_attn_dist, p_gens

    def get_model(self, pad_id):
        self.pad_id = pad_id
        inp_src = Input(name='src_input',
                      shape=(self.args.src_seq_length, ), 
                      dtype='int32'
                     )
        inp_src_extend_oov = Input(name='src_input_ext',
                      shape=(self.args.src_seq_length, ), 
                      dtype='int32'
                     )

        inp_tar = Input(name='tar_input',
                            shape=(self.args.tar_seq_length, ), 
                            dtype='int32',
                           )
        inp_tar_loss = Input(name='tar_input_loss',
                            shape=(self.args.tar_seq_length, ), 
                            dtype='int32',
                           )
        inp_tar_loss_extend_oov = Input(name='tar_input_loss_exp',
                            shape=(self.args.tar_seq_length, ), 
                            dtype='int32',
                           )

        inp_facts = Input(name='facts_input',
                            shape=(self.args.fact_number, self.args.src_seq_length,), 
                            dtype='int32',
                           )
        inp_facts_extend_oov = Input(name='facts_input_ext',
                            shape=(self.args.fact_number, self.args.src_seq_length,), 
                            dtype='int32',
                           )

        #tar_padding_mask = PGPaddingMaskLayer(name='tar_padding_mask', pad_id=self.pad_id)(inp_tar_loss_extend_oov)
        tar_padding_mask = PGPaddingMaskLayer(name='tar_padding_mask', pad_id=self.pad_id)(inp_tar_loss)

        # shape: (bs, sf_number, seq_len)
        inp_src_normal = Lambda(lambda x: K.expand_dims(x, axis=1))(inp_src)
        inp_tar_normal = Lambda(lambda x: K.expand_dims(x, axis=1))(inp_tar)

        src_encoder_output = self.__get_encoder(inp_src_normal, 'src')
        fact_encoder_output = self.__get_encoder(inp_facts, 'fact')

        mutual_tar_src_mask = PaddingMaskLayer(name='mutual_tar_src_mask', src_len=self.args.tar_seq_length,
                                            pad_id=self.pad_id)(inp_src_normal)
        mutual_tar_fact_mask = PaddingMaskLayer(name='mutual_tar_fact_mask', src_len=self.args.tar_seq_length,
                                            pad_id=self.pad_id)(inp_facts)
        mutual_src_dec_ctxt_mask = PaddingMaskLayer(name='mutual_src_dec_ctxt_mask', src_len=self.args.src_seq_length,
                                            pad_id=self.pad_id)(inp_tar_normal)
        mutual_fact_dec_ctxt_mask = PaddingMaskLayer(name='mutual_fact_dec_ctxt_mask', src_len=self.args.src_seq_length,
                                            pad_id=self.pad_id)(inp_tar_normal)

        #decoder_output, src_attn_dist, p_gens = self.__get_decoder(
        decoder_output, src_attn_dist, fact_attn_dist, p_gens = self.__get_decoder(
            inp_tar_normal, 
            src_encoder_output, mutual_tar_src_mask,
            fact_encoder_output, mutual_tar_fact_mask,
            mutual_src_dec_ctxt_mask,
            mutual_fact_dec_ctxt_mask,
        )
        #src_attn_dist = self.pg_get_src_dist_layer(src_attn_dist) 

        print('decoder_output: ', decoder_output)
        decoder_output = Reshape((self.args.tar_seq_length, self.args.embedding_dim, ))(decoder_output)
        # build model part
        gen_word_pred = self.output_softmax_layer(
            self.output_layer([decoder_output, self.decoder_embedding_matrix])
        )
        final_word_pred = self.pg_word_predict_layer([gen_word_pred, src_attn_dist, fact_attn_dist, p_gens, \
            inp_src_extend_oov, inp_facts_extend_oov])
            #inp_src, inp_facts])
        #loss = self.pg_loss_layer([final_word_pred, inp_tar_loss_extend_oov, tar_padding_mask])
        #loss = self.pg_loss_layer([final_word_pred, inp_tar_loss, tar_padding_mask])
        model = Model(
                      inputs=[inp_src, inp_tar, inp_facts, inp_src_extend_oov, inp_facts_extend_oov],
                      outputs=final_word_pred,
                      #outputs=[loss, final_word_pred],
                      #inputs=[inp_src, inp_tar, inp_facts, inp_src_extend_oov, inp_tar_extend_oov, inp_facts_extend_oov],
                      #inputs=[inp_src, inp_tar, inp_facts, inp_src_extend_oov, inp_facts_extend_oov],
                      #outputs=loss,
                     )
        return model

get_custom_objects().update({
    'VSPGTransformerModel': VSPGTransformerModel,
})
