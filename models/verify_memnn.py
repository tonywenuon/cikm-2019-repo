#!/usr/bin/env python
# coding: utf-8

'''
Compare with the results from the repo: https://github.com/domluna/memn2n
This repo shows its results on bAbI data set. We can use the final results to verify our results.
'''


# In[1]:

import os, sys, time, math

import tensorflow as tf
from keras.utils.data_utils import get_file
import tarfile
import collections
import re, argparse
import numpy as np
from tensorflow.keras import backend as K
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from sklearn import metrics
from keras_tqdm import TQDMNotebookCallback
from memnn import MemNNModel
from tensorflow.keras.models import load_model



# In[2]:


def tokenize(sent):
    return [x.strip() for x in re.split('(\W+)?', sent) if x.strip()]

def parse_stories(lines):
    data = []
    story = []
    for line in lines:
        line = line.decode('utf-8').strip()
        nid, line = line.split(' ', 1)
        if int(nid) == 1: story = []
        if '\t' in line:
            q, a, supporting = line.split('\t')
            q = tokenize(q)
            substory = None
            substory = [[str(i)+":"]+x for i,x in enumerate(story) if x]
            data.append((substory, q, a))
            story.append('')
        else: story.append(tokenize(line))
    return data

path = get_file('babi-tasks-v1-2.tar.gz', 
                origin='https://s3.amazonaws.com/text-datasets/babi_tasks_1-20_v1-2.tar.gz')
tar = tarfile.open(path)
challenges = { 
    # QA1 with 10,000 samples
    'single_supporting_fact_10k': 'tasks_1-20_v1-2/en-10k/qa1_single-supporting-fact_{}.txt',
    # QA2 with 10,000 samples
    'two_supporting_facts_10k': 'tasks_1-20_v1-2/en-10k/qa2_two-supporting-facts_{}.txt',
    'two_supporting_facts_1k': 'tasks_1-20_v1-2/en/qa2_two-supporting-facts_{}.txt',
}
challenge_type = 'single_supporting_fact_10k'
challenge = challenges[challenge_type]

def get_stories(f):
    data = parse_stories(f.readlines())
    return [(story, q, answer) for story, q, answer in data]

train_stories = get_stories(tar.extractfile(challenge.format('train')))
test_stories = get_stories(tar.extractfile(challenge.format('test')))
stories = train_stories + test_stories
story_maxlen = max((len(s) for x, _, _ in stories for s in x))
story_maxsents = max((len(x) for x, _, _ in stories))
query_maxlen = max(len(x) for _, x, _ in stories)

def do_flatten(el): 
    return isinstance(el, collections.Iterable) and not isinstance(el, (str, bytes))
def flatten(l):
    for el in l:
        if do_flatten(el): yield from flatten(el)
        else: yield el
vocab = sorted(set(flatten(stories)))
vocab.insert(0, '<PAD>')
vocab.insert(1, '<START>')
vocab.insert(2, '<END>')
vocab_size = len(vocab)

print('-' * 100)
print(story_maxsents, vocab_size, story_maxlen, query_maxlen, len(train_stories), len(test_stories))
#print(test_stories[534])

word_idx = dict((c, i) for i, c in enumerate(vocab))
idx_word = dict((i, c) for i, c in enumerate(vocab))

pad_id = word_idx['<PAD>']
start_id = word_idx['<START>']
end_id = word_idx['<END>']
def vectorize_stories(data, word_idx, story_maxlen, query_maxlen):
    X = []; Xq = []; Y = []; Y_loss = []
    for story, query, answer in data:
        x = [[word_idx[w] for w in s] for s in story]
        xq = [word_idx[w] for w in query]
        xq = [start_id] + xq + [end_id]
        y = [word_idx[answer]]
        y = [start_id] + y 
        y_loss = y + [end_id]
        X.append(x); Xq.append(xq); Y.append(y); Y_loss.append(y_loss)
    return ([pad_sequences(x, maxlen=story_maxlen + 2) for x in X],
            pad_sequences(Xq, maxlen=query_maxlen + 2), 
            pad_sequences(Y, maxlen=2),
            pad_sequences(Y_loss, maxlen=2))

S, Q, A, A_loss = vectorize_stories(train_stories, word_idx, story_maxlen, query_maxlen)

inputs_train, val_S, queries_train, val_Q, answers_train, val_A, answers_train_loss, val_A_loss = \
    train_test_split(S, Q, A, A_loss, test_size=.1)
inputs_test, queries_test, answers_test, answers_test_loss = vectorize_stories(test_stories, word_idx, story_maxlen, query_maxlen)

answers_train_loss=np.expand_dims(np.asarray(answers_train_loss), axis=-1)
val_A_loss=np.expand_dims(np.asarray(val_A_loss), axis=-1)
answers_test_loss=np.expand_dims(np.asarray(answers_test_loss), axis=-1)


def stack_inputs(inputs):
    for i,it in enumerate(inputs):
        inputs[i] = np.concatenate([it, 
                                    np.zeros((story_maxsents-it.shape[0], story_maxlen + 2), 'int')])
    return np.stack(inputs)
inputs_train = stack_inputs(inputs_train)
inputs_test = stack_inputs(inputs_test)

print('Done!')

# In[4]:

def memnn_add_arguments(parser):
    parser.add_argument("--data_set", default='wizard', type=str, help="Currently, this can be wizard or reddit")
    parser.add_argument("--exp_name", default='memnn', type=str, help="The experiment name.")
    parser.add_argument("--epochs", default=100, type=int, help="Epoch numbers.")
    parser.add_argument("--batch_size", default=50,type=int, help="Batch size.")
    parser.add_argument("--hops", default=3,type=int, help="How many hops in the MemNN.")
    parser.add_argument("--src_seq_length", default=30,type=int, help="Source sequence length")
    parser.add_argument("--tar_seq_length", default=30,type=int, help="Target sequence length")
    parser.add_argument("--fact_seq_length", default=30,type=int, help="Fact sequence length")
    parser.add_argument("--embedding_dim", default=100, type=int, help="Word embedding size.")
    parser.add_argument("--hidden_dim", default=100, type=int, help="GRU hidden state size.")
    parser.add_argument("--vocab_size", default=50000, type=int, help="Vocabulary size.")
    parser.add_argument("--early_stop_patience", default=3, type=int, help="Indicate how many step to show current loss.")
    parser.add_argument("--lr_min", default=0.005, type=float, help="When learning rate decays to lr_min, it stops.")
    parser.add_argument("--lr_decay_patience", default=2, type=float, help="When lr doesn't change for this number, it begin to decay'.")
    parser.add_argument("--fact_number", default=10, type=int, help="Fact number, required by the data reader class.")
    parser.add_argument("--conv_number", default=10, type=int, help="Conversation number, required by the data reader class.")
    parser.add_argument("--checkpoints_dir", default='log', type=str, help="The folder to save checkpoint.")
    parser.add_argument("--outputs_dir", default='outputs', type=str, help="The folder to save test outputs file.")


# In[5]:



parser = argparse.ArgumentParser()
memnn_add_arguments(parser)
args = parser.parse_args(args=[])
print('Processing')
args.src_seq_length = query_maxlen + 2
args.fact_seq_length = story_maxlen + 2
args.fact_number = story_maxsents
args.tar_seq_length = 2
args.vocab_size = vocab_size
args.batch_size = 10
args.epochs = 3
args.embedding_dim = 20
args.hidden_dim = 20
print('Done')


# In[6]:


print('*' * 100)
print('queries_train: ', (queries_train.shape))
print('inputs_train: ', (inputs_train.shape))
print('answers_train: ', (answers_train.shape))
print('*' * 100)

inps = [queries_train, inputs_train, answers_train]
val_inps = [val_Q, val_S, val_A]


# In[7]:


test_preds = []
test_labels = []


# In[8]:


def test(model, test_src_ids, test_facts_ids, test_tar_ids):
    def __get_batch():
        batch_src = []
        batch_facts = []
        batch_tar = []
        for (src_input, facts_input, tar_input) in zip(test_src_ids, test_facts_ids, test_tar_ids):
            batch_src.append(src_input)
            batch_facts.append(facts_input)
            batch_tar.append(tar_input)
            if len(batch_src) == args.batch_size:
                res = (np.asarray(batch_src), np.asarray(batch_facts), np.asarray(batch_tar))
                batch_src = []
                batch_facts = []
                batch_tar = []
                yield res[0], res[1], res[2]
        if len(batch_src) != 0:
            yield np.asarray(batch_src), np.asarray(batch_facts), np.asarray(batch_tar)


    for (batch, (src_input, facts_input, tar_input)) in         enumerate(__get_batch()):
        if batch % 20 == 0:
            print('Current batch: {}/{}. '.format(batch + 1, len(test_src_ids) // args.batch_size))
        cur_batch_size = tar_input.shape[0]
        tar_length = tar_input.shape[1]

        results = []
        results = np.zeros((cur_batch_size, tar_length), dtype='int32')
        results[:, 0] = start_id
        for i in range(1, tar_length):
            results[:, i] = pad_id
        for t in range(1, tar_length):
            preds = model.predict([src_input, facts_input, results]) # shape: (batch_size, tar_length, vocab_size)
            pred_id = np.argmax(preds, axis=-1)
            results[:, t] = pred_id[:, t - 1]

        def calc_results(_type, outputs):
            for result in outputs:
                seq = []
                for _id in result:
                    _id = int(_id)
                    if _id == end_id:
                        break
                    if _id != pad_id and _id != start_id:
                        seq.append(idx_word.get(_id, '<UNK>'))
                write_line = ' '.join(seq)
                if _type == 'pred':
                    test_preds.append(write_line.strip())
                elif _type == 'target':
                    test_labels.append(write_line.strip())

        calc_results('pred', results)
        calc_results('target', tar_input)
        


# In[10]:

#import keras.backend.tensorflow_backend as KTF
#KTF.set_session(tf.Session(config=tf.ConfigProto(device_count={'cpu':0})))  

os.environ["CUDA_VISIBLE_DEVICES"] = "0"

mem_model = MemNNModel(args)
answer, _, _ = mem_model.get_model()
opt = tf.keras.optimizers.Adam(lr=0.003, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)
answer.compile(optimizer=opt, loss=tf.keras.losses.sparse_categorical_crossentropy)

K.set_value(answer.optimizer.lr, 1e-2)
hist=answer.fit(inps, answers_train_loss, epochs=args.epochs, batch_size=args.batch_size,
                validation_split=0.1)

test(answer, queries_test, inputs_test, answers_test)
test_acc = metrics.accuracy_score(test_preds, test_labels)
# final accuracy. Compare this score with the score table in https://github.com/domluna/memn2n
print(test_acc)


# In[ ]:




